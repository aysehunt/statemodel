import java.util.*;

public class FortuneTeller {
	

	private static Scanner scan;

	public static void main(String[] args) {
		scan = new Scanner(System.in);
		System.out.println("Welcome to my fortune teller!");
		
		System.out.println("Pick a number!");
		System.out.println("1, 2, 3, 4");
		String number = scan.next();
		
		System.out.println("Pick a color!");
		System.out.println("Red, Yellow, Blue, Violet");
		String color = scan.next();
		
		switch (color){
			case "Red": 
					System.out.println("1...2...3");
			        break;
			case "Yellow": 
					System.out.println("1...2...3...4...5...6");
			        break;       
			case "Blue": 
					System.out.println("1...2...3...4");
					break;      
			case "Violet":
					System.out.println("1...2...3...4...5...6");
					break;
		}
		
		System.out.println("Okay, so lets review--you chose "+number+" and "
				+color);	
		System.out.println("Below is your fortune!");
		
		switch(number){
			case "1": System.out.println("You are about to meet someone who has the potential to "+
					"be the love of your life.\n This will only be possible if you are able to let them in.");
					 break;
			case "2":System.out.println("You may have been wrestling with a question about your identity or your future. "+
					"\nAlthough it may be difficult, it is important to be true to yourself.");
					 break;
			case "3":System.out.println("With every new year comes a bevy of New Year's revolutions. "+
					 "\nIt's easy to spread yourself too thin, try focusing on one goal and giving it your all.");
					 break;
			case "4":System.out.println("You many have had a falling out with a friend recently. "+
					 "\nConventional wisdom would suggest you try to mend the relatinship, but if that person has stopped bringing you joy it's okay to cut them out.");
					 break;
		}
		
	}

}
